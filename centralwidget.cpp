/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "centralwidget.h"

#include "cmd/cmdprocessor.h"
#include "document/document.h"
#include "document/documentview.h"
#include "document/project.h"
#include "niy.h"
#include "tools/toolsbox.h"
#include "toolsdrawer.h"

#include <QApplication>
#include <QBoxLayout>
#include <QClipboard>
#include <QColor>
#include <QDebug>
#include <QFileDialog>
#include <QFileInfo>
#include <QGraphicsDropShadowEffect>
#include <QGraphicsScene>
#include <QImageReader>
#include <QImageWriter>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QSplitter>
#include <QStatusBar>
#include <QToolBar>
#include <QToolButton>

CentralWidget::CentralWidget(QWidget *parent)
    : QWidget(parent)
    , m_ribbon(new QToolBar(this))
    , m_tools(new ToolsBox(this))
    , m_toolsDrawer(new ToolsDrawer(this))
    , m_scene(new QGraphicsScene(this))
    , m_doc(Project::createDoc(this))
    , m_docView(Project::createView(this))
    , m_statusBar(new QStatusBar(this))
{
    m_ribbon->setFixedHeight(64);

    QSplitter *hSplitter = new QSplitter(this);
    hSplitter->setOrientation(Qt::Horizontal);
    hSplitter->addWidget(m_docView);
    hSplitter->addWidget(m_toolsDrawer);
    hSplitter->setStretchFactor(0, 20);
    hSplitter->setStretchFactor(1, 0);
    hSplitter->setHandleWidth(0);
    hSplitter->setContentsMargins(0, 0, 0, 0);

    QVBoxLayout *root = new QVBoxLayout(this);
    root->addWidget(m_ribbon);
    root->addWidget(hSplitter);
    root->addWidget(m_statusBar);
    root->setStretchFactor(m_ribbon, 1);
    root->setStretchFactor(hSplitter, 20);
    root->setContentsMargins(0, 0, 0, 0);
    root->setSpacing(0);

    m_statusBar->setFixedHeight(65);

    m_docView->setScene(m_scene);
    m_docView->setDocument(m_doc);

    m_tools->initTools(m_docView);

    initToolBar();
    initToolBox();

    CmdProcessor::init(m_doc, this);

    m_doc->open(":/img/img_stock/photo.png");
}

QGraphicsEffect *createShadowEffect()
{
    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();
    shadow->setOffset(0, 4);
    shadow->setBlurRadius(16);
    QColor shadowColor(Qt::black);
    shadowColor.setAlphaF(0.25);
    shadow->setColor(shadowColor);

    return shadow;
}

void CentralWidget::initToolBar()
{
    QMenu *menu = new QMenu(tr("&File"));
    menu->setObjectName("menuFile");
    menu->setWindowFlags(menu->windowFlags() | Qt::FramelessWindowHint | Qt::NoDropShadowWindowHint);
    menu->setAttribute(Qt::WA_TranslucentBackground);
    menu->setGraphicsEffect(::createShadowEffect());

    QAction *actOpen = menu->addAction(tr("Open file"), this, &CentralWidget::openFile, QKeySequence::Open);
    menu->addSeparator();
    /*QAction* actSave =*/menu->addAction(tr("Save"), this, &CentralWidget::save, QKeySequence::Save);
    /*QAction* actSaveAs =*/menu->addAction(tr("Save as…"), this, &CentralWidget::saveAs, QKeySequence::SaveAs);
    menu->addSeparator();
    /*QAction* actPrint =*/menu->addAction(tr("Print"), this, &CentralWidget::print, QKeySequence::Print);

    QToolButton *fileButton = new QToolButton(m_ribbon);
    fileButton->setObjectName("btnFileMenu");
    fileButton->setMenu(menu);
    fileButton->setDefaultAction(actOpen);
    fileButton->setText(tr("File"));
    fileButton->setPopupMode(QToolButton::MenuButtonPopup);

    m_ribbon->setObjectName("ribbon");
    m_ribbon->addWidget(fileButton);

    auto createToolButton = [this](QAction *act, const QString &objName, const QKeySequence &key = QKeySequence()) {
        act->setShortcut(key);
        act->setObjectName(objName);
        QToolButton *btn = new QToolButton();
        btn->setDefaultAction(act);
        btn->setObjectName(QString("%1_btn").arg(objName));
        m_ribbon->addWidget(btn);

        return act;
    };

    createToolButton(m_doc->commandsStack()->createUndoAction(this), "actUndo", QKeySequence::Undo);
    createToolButton(m_doc->commandsStack()->createRedoAction(this), "actRedo", QKeySequence::Redo);
    QAction *actReset = createToolButton(new QAction(tr("Reset"), this), "actReset");
    actReset->setEnabled(m_doc->isLoaded());
    connect(m_doc, &Document::documentLoaded, actReset, &QAction::setEnabled);
    connect(actReset, &QAction::triggered, m_doc, &Document::reset);

    QWidget *spacer = new QWidget(m_ribbon);
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    m_ribbon->addWidget(spacer);

    QPushButton *btnCopy = new QPushButton(tr("Copy"));
    btnCopy->setObjectName("btnCopy");
    m_ribbon->setIconSize({ 20, 22 });
    btnCopy->setIconSize({ 20, 22 });
    /*QAction *actCopy = */ m_ribbon->addWidget(btnCopy);

    connect(btnCopy, &QPushButton::clicked, this, &CentralWidget::copyToClipboard);
}

void CentralWidget::initToolBox()
{
    for (auto tool : m_tools->tools())
        m_toolsDrawer->addTool(tool);
}

QString getKnownFormats(bool out)
{
    const QList<QByteArray> &formats =
            out ? QImageWriter::supportedImageFormats() : QImageReader::supportedImageFormats();
    QStringList extensions;
    for (const auto &format : formats)
        extensions.append(QString("*.%1").arg(QString::fromUtf8(format).toLower()));

    return QString("Images (%1);;Other files(*.*)").arg(extensions.join(" "));
}

void CentralWidget::openFile()
{
    if (!ensureSaved())
        return;

    const QString &path = QFileDialog::getOpenFileName(this, "Open file", m_doc->filePath(), getKnownFormats(false));
    if (!path.isEmpty()) {
        m_doc->open(path);
    }
}

QString imgFormatFromName(const QString &fileName)
{
    if (fileName.isEmpty())
        return {};

    QFileInfo fi(fileName);
    return QString("*.%1").arg(fi.completeSuffix());
}

bool CentralWidget::save()
{
    const QString &currName = m_doc->filePath();
    if (currName.isEmpty())
        return saveAs();
    else
        return m_doc->save(currName);

    return false;
}

bool CentralWidget::saveAs()
{
    QString currName = m_doc->filePath();
    QString filter = imgFormatFromName(currName);
    currName = QFileDialog::getSaveFileName(this, tr("Save as..."), currName, getKnownFormats(true));

    if (!currName.isEmpty())
        return m_doc->save(currName);

    return false;
}

bool CentralWidget::print()
{
    utj::showNIY("Print", this);
    return false;
}

bool CentralWidget::ensureSaved()
{
    bool saved = true;
    if (!m_doc->isLoaded() || !m_doc->isDirty())
        return saved;

    const QString &question = tr("The image%1 has been modified.<br>"
                                 "Do you want to save changes or discard them?");
    const QFileInfo fi(m_doc->filePath());

    QString docName;
    if (fi.exists())
        docName = QString(" '%1'").arg(fi.fileName());

    switch (QMessageBox::question(nullptr, tr("Close Image"), question.arg(docName),
                                  QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel)) {
    case QMessageBox::Save:
        saved = save();
        break;
    case QMessageBox::Discard:
        saved = true;
        break;
    case QMessageBox::Cancel:
    default:
        saved = false;
        break;
    }

    return saved;
}

void CentralWidget::copyToClipboard()
{
    const QPixmap &pm = m_doc->renderPixmap(Qt::OpaqueMode);
    qApp->clipboard()->setPixmap(pm);
}
