/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include <QWidget>

class QGraphicsScene;
class Document;
class DocumentView;
class ToolsDrawer;
class ToolsBox;
class QToolBar;
class QStatusBar;

class CentralWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CentralWidget(QWidget *parent = nullptr);

protected:
    QToolBar *m_ribbon;
    ToolsBox *m_tools { nullptr };
    ToolsDrawer *m_toolsDrawer { nullptr };

    QGraphicsScene *m_scene { nullptr };
    Document *m_doc { nullptr };
    DocumentView *m_docView { nullptr };

    QStatusBar *m_statusBar { nullptr };

    void initToolBar();
    void initToolBox();

    bool ensureSaved();

protected slots:
    void openFile();
    bool save();
    bool saveAs();
    bool print();
    void copyToClipboard();
};
