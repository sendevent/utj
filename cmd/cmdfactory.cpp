/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "cmdfactory.h"

#include "cmd/cmdopacitychange.h"
#include "cmd/cmdpencolorchange.h"
#include "tools/pentool.h"

#include <QDebug>
#define WRN qWarning()

/*static*/ QUndoCommand *CmdFactory::createCommand(utj::Tool tt, const QVariantList &params, const DocumentPtr &doc)
{
    switch (tt) {
    case utj::Tool::OpacityChange:
        return cmdCreateOpacityChange(params, doc);
    case utj::Tool::ChangePenColor:
        return cmdCreatePenColorChange(params);
    default:
        WRN << "unhandled tool/command: " << tt;
        break;
    }

    return nullptr;
}

/*static*/ QUndoCommand *CmdFactory::cmdCreateOpacityChange(const QVariantList &params, const DocumentPtr &doc)
{
    if (params.length() == 1) {
        const int opacity = params[0].toInt();
        return new CmdOpacityChange(opacity, doc.data());
    }
    return nullptr;
}

/*static*/ QUndoCommand *CmdFactory::cmdCreatePenColorChange(const QVariantList &params)
{
    if (params.length() == 2) {
        const QColor &c = params[0].value<QColor>();
        PenTool *pen = params[1].value<PenTool *>();
        return new CmdPenColorChange(c, pen);
    }
    return nullptr;
}
