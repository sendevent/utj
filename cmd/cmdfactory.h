/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include "document/document.h"
#include "tools/tooltype.h"

#include <QVariantList>

class QUndoCommand;

class CmdFactory
{
public:
    static QUndoCommand *createCommand(utj::Tool tt, const QVariantList &params, const DocumentPtr &doc);

private:
    CmdFactory() {};

    static QUndoCommand *cmdCreateOpacityChange(const QVariantList &params, const DocumentPtr &doc);
    static QUndoCommand *cmdCreatePenColorChange(const QVariantList &params);
};
