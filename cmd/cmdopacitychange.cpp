/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "cmdopacitychange.h"

#include "tools/tooltype.h"

#include <QDebug>

CmdOpacityChange::CmdOpacityChange(int opacity, Document *pDoc, QUndoCommand *parent)
    : QUndoCommand(QObject::tr("Opacity %1").arg(QString::number(opacity)), parent)
    , m_doc(pDoc)
    , m_opacityNew(opacity)
    , m_opacityPrev(m_doc->opacity())
{
}

void CmdOpacityChange::redo()
{
    qDebug() << "redo to" << m_opacityNew;
    m_doc->setOpacity(m_opacityNew);
}

void CmdOpacityChange::undo()
{
    qDebug() << "undo to" << m_opacityPrev;
    m_doc->setOpacity(m_opacityPrev);
}

int CmdOpacityChange::id() const
{
    return utj::OpacityChange;
}
