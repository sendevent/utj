/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include "document/document.h"

#include <QUndoCommand>

class CmdOpacityChange : public QUndoCommand
{
public:
    explicit CmdOpacityChange(int opacity, Document *pDoc, QUndoCommand *parent = nullptr);

    void redo() override;
    void undo() override;
    int id() const override;
    bool mergeWith(const QUndoCommand *) override { return false; }

protected:
    DocumentPtr m_doc;

    int m_opacityNew;
    int m_opacityPrev;
};
