/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "cmdpencolorchange.h"

#include "tools/pentool.h"
#include "tools/tooltype.h"

CmdPenColorChange::CmdPenColorChange(const QColor &c, PenTool *pen, QUndoCommand *parent)
    : QUndoCommand(parent)
    , m_penTool(pen)
    , m_colorNew(c)
    , m_colorPrev(m_penTool->color())
{
    setText(QObject::tr("Change color %1").arg(m_colorNew.name()));
}

void CmdPenColorChange::redo()
{
    m_penTool->setColor(m_colorNew);
}

void CmdPenColorChange::undo()
{
    m_penTool->setColor(m_colorPrev);
}

int CmdPenColorChange::id() const
{
    return utj::ChangePenColor;
}
