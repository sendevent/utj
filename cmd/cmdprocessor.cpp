/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "cmdprocessor.h"

#include "cmd/cmdfactory.h"

#include <QUndoStack>

/*static*/ CmdProcessor *CmdProcessor::m_instance = nullptr;

CmdProcessor::CmdProcessor(Document *doc, QObject *parent)
    : QObject(parent)
    , m_doc(doc)
{
}

/*static*/ void CmdProcessor::init(Document *doc, QObject *parent)
{
    if (!m_instance)
        m_instance = new CmdProcessor(doc, parent);
}

/*static*/ DocumentPtr CmdProcessor::runCmd(utj::Tool tool, const QVariantList &params)
{
    if (m_instance) {
        if (QUndoCommand *cmd = CmdFactory::createCommand(tool, params, m_instance->m_doc)) {
            m_instance->m_doc->commandsStack()->push(cmd);
            return m_instance->m_doc;
        }
    }

    return nullptr;
}
