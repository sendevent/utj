/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include "document/document.h"
#include "tools/tooltype.h"

#include <QObject>
#include <QVariantList>

class CmdProcessor : public QObject
{
    Q_OBJECT
public:
    static void init(Document *doc, QObject *parent);

    static DocumentPtr runCmd(utj::Tool tool, const QVariantList &params);

protected:
    static CmdProcessor *m_instance;
    explicit CmdProcessor(Document *doc, QObject *parent);

    DocumentPtr m_doc;
};
