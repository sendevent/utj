/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "cssloader.h"

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QKeyEvent>

CssLoader::CssLoader(const QString &dirPath, const QKeySequence &key, QObject *parent)
    : QObject(parent)
    , m_dirPath(dirPath)
    , m_trigger(key)
    , m_target(parent)
{
    m_target->installEventFilter(this);
}

void CssLoader::attach(QObject *target, const QString &filename, const QKeySequence &key)
{
    CssLoader *loader = new CssLoader(filename, key, target);
    loader->apply();
}

QString CssLoader::readDir() const
{
    QString css;

    const QDir root(m_dirPath);
    for (const QFileInfo &fi : root.entryInfoList(QDir::Files | QDir::Readable))
        if (fi.suffix().toLower().endsWith("css")) {
            static const QString commentTemplate("/** %1 **/\n\n");
            css.append(commentTemplate.arg(fi.fileName()));
            css.append(readFile(fi.absoluteFilePath()));
        }

    return css;
}

void CssLoader::apply()
{
    const QString &css = readDir();
    if (!css.isEmpty()) {
        if (auto app = qobject_cast<QApplication *>(m_target))
            app->setStyleSheet(css);
        // TODO: handle installing on a widget
    }
}

QString CssLoader::readFile(const QString &absPath) const
{
    qDebug() << "reading " << absPath;
    QFile f(absPath);
    if (f.open(QFile::ReadOnly | QFile::Text))
        return QString::fromUtf8(f.readAll());

    qWarning() << "Failed reading " << absPath << ": " << f.errorString();
    return QString();
}

bool CssLoader::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if (QKeySequence(keyEvent->key()) == m_trigger) {
            apply();
            return true;
        }
    }

    return QObject::eventFilter(obj, event);
}
