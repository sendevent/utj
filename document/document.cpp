/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "document.h"

#include <QDebug>
#include <QPainter>
#include <QPixmap>

#define LOG qDebug()
#define WRN qWarning()
#define WRN_NIY WRN << "Not implemented yet"

Document::Document(QObject *parent)
    : QObject(parent)
    , m_commands(new QUndoStack(this))
{
}

bool Document::open(const QString &filePath)
{
    if (m_img)
        delete m_img;

    m_img = new QImage(filePath);
    m_filePath = filePath;

    reset();

    const bool loaded = updatePixmap();

    emit documentLoaded(loaded);

    return loaded;
}

void Document::reset()
{
    setOpacity(100);
    m_commands->setClean();
}

bool Document::save(const QString &filePath)
{
    const QPixmap &pm = renderPixmap(Qt::OpaqueMode);
    const bool ok = pm.save(filePath);
    if (ok) {
        m_filePath = filePath;
        m_commands.clear();
    }

    return ok;
}

bool Document::isDirty() const
{
    return !m_commands->isClean();
}

bool Document::isLoaded() const
{
    return !m_pm.isNull();
}

int Document::opacity() const
{
    return m_opacity;
}

void Document::setOpacity(int opacity)
{
    if (opacity != m_opacity) {
        m_opacity = qMax(0, qMin(100, opacity));

        qDebug() << opacity << m_opacity;
        updatePixmap();

        emit opacityChanged(m_opacity);
        requestUpdateView();
    }
}

void Document::requestUpdateView()
{
    // TODO: ensure there is a change actually
    emit renderRequest();
}

QPixmap Document::renderPixmap(Qt::BGMode mode) const
{
    if (!m_img)
        return {};

    QImage img(m_img->size(), m_img->format());
    img.fill(Qt::transparent);
    const QRect &r = m_img->rect();

    const qreal factor = m_opacity ? qreal(m_opacity) / 100. : 0.;

    QPainter p(&img);
    if (mode == Qt::TransparentMode) {
        if (m_opacity < 100) {
            p.save();
            // TODO: handle this on the view level -
            // to avoid redundant rendering on copy/save
            static const QPixmap &pattern = []() {
                const int side = 10;
                QPixmap pm(2 * side, 2 * side);
                QPainter p(&pm);
                p.fillRect(0, 0, side, side, Qt::lightGray);
                p.fillRect(side, side, side, side, Qt::lightGray);
                p.fillRect(0, side, side, side, Qt::darkGray);
                p.fillRect(side, 0, side, side, Qt::darkGray);
                return pm;
            }();
            p.fillRect(r, QBrush(pattern));
            p.restore();
        }
    }
    p.setOpacity(factor);
    p.drawImage(r, *m_img, r);

    return QPixmap::fromImage(img);
}

bool Document::updatePixmap(Qt::BGMode mode)
{
    m_pm = renderPixmap(mode);
    return !m_pm.isNull();
}
