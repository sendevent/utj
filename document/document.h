/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include <QObject>
#include <QPixmap>
#include <QPointer>
#include <QUndoStack>

class Document : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int Opacity READ opacity WRITE setOpacity NOTIFY opacityChanged)

public:
    explicit Document(QObject *parent = nullptr);

    QString filePath() const { return m_filePath; }

    bool open(const QString &filePath);
    bool save(const QString &filePath);

    bool isDirty() const;

    bool isLoaded() const;

    QPointer<QUndoStack> commandsStack() const { return m_commands; }

    QPixmap getPixmap() const { return m_pm; }
    QPixmap renderPixmap(Qt::BGMode mode = Qt::TransparentMode) const;

    QImage *image() const { return m_img; }

    int opacity() const;
    void setOpacity(int opacity);

signals:
    void documentLoaded(bool);
    void opacityChanged(int);

    void renderRequest();

protected:
    QString m_filePath;
    QPointer<QUndoStack> m_commands;
    QImage *m_img { nullptr };
    QPixmap m_pm;

    int m_opacity { 100 };

    bool updatePixmap(Qt::BGMode mode = Qt::TransparentMode);

private:
    void requestUpdateView();

public slots:
    void reset();
};

typedef QPointer<Document> DocumentPtr;
