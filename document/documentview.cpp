/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "documentview.h"

#include "canvasitem.h"

#include <QDebug>

DocumentView::DocumentView(QWidget *parent)
    : QGraphicsView(parent)
    , m_item(new CanvasItem())
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

DocumentView::~DocumentView()
{
    detachCanvasItem();
}

bool DocumentView::detachCanvasItem()
{
    if (auto s = scene()) {
        s->removeItem(m_item);

        delete m_item;
        m_item = nullptr;

        return true;
    }

    return false;
}

void DocumentView::setScene(QGraphicsScene *pScene)
{
    if (detachCanvasItem())
        m_item = new CanvasItem();

    QGraphicsView::setScene(pScene);
    if (auto s = scene())
        s->addItem(m_item);
}

void DocumentView::setDocument(const DocumentPtr &doc)
{
    if (doc == m_doc)
        return;

    if (m_doc) {
        disconnect(m_doc, &Document::renderRequest, this, &DocumentView::setupPixmap);
        disconnect(m_doc, &Document::documentLoaded, this, &DocumentView::setupPixmap);
    }

    m_doc = doc;

    if (m_doc) {
        connect(m_doc, &Document::documentLoaded, this, &DocumentView::setupPixmap);
        connect(m_doc, &Document::renderRequest, this, &DocumentView::setupPixmap);
        setupPixmap();
    }
}

void DocumentView::setupPixmap()
{
    const QPixmap &pm = m_doc->getPixmap();
    m_item->setPixmap(pm);
    polishImage();
}

QColor DocumentView::colorAt(const QPoint &globalPos) const
{
    const QPoint &myPixel = mapFromGlobal(globalPos);
    const QPoint &itemPixel = m_item->mapFromScene(mapToScene(myPixel)).toPoint();
    return m_doc->image()->pixel(itemPixel);
}

void DocumentView::resizeEvent(QResizeEvent *e)
{
    QGraphicsView::resizeEvent(e);
    polishImage();
}

void DocumentView::polishImage()
{
    fitInView(m_item, Qt::KeepAspectRatio);
    centerOn(m_item);
}
