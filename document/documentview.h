/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include "document.h"

#include <QGraphicsView>

class QGraphicsScen;
class CanvasItem;

class DocumentView : public QGraphicsView
{
    Q_OBJECT
public:
    DocumentView(QWidget *parent = nullptr);
    ~DocumentView();

    void setScene(QGraphicsScene *pScene);
    void setDocument(const DocumentPtr &doc);

    QColor colorAt(const QPoint &globalPos) const;

    CanvasItem *item() { return m_item; }

protected:
    DocumentPtr m_doc;
    CanvasItem *m_item { nullptr };

    bool detachCanvasItem();

    void resizeEvent(QResizeEvent *e) override;

    void polishImage();

protected slots:
    void setupPixmap();
};
