/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "project.h"

#include "documentview.h"

/*static*/ Project *Project::m_instance = nullptr;

Project::Project(QObject *parent)
    : QObject(parent)
    , m_doc(nullptr)
    , m_view(nullptr)
{
}

/*static*/ Document *Project::createDoc(QObject *parent)
{
    instance()->m_doc = new Document(parent);
    return instance()->m_doc;
}

/*static*/ DocumentView *Project::createView(QWidget *parent)
{
    instance()->m_view = new DocumentView(parent);
    return instance()->m_view;
}

/*static*/ DocumentPtr Project::currentDoc()
{
    return instance()->m_doc;
}

/*static*/ DocumentView *Project::mainView()
{
    return instance()->m_view;
}

/*static*/ Project *Project::instance()
{
    if (!m_instance)
        m_instance = new Project();
    return m_instance;
}
