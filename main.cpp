/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "cssloader.h"
#include "mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFontDatabase>
#include <QKeySequence>

void loadFlonts()
{
    for (const auto &fontFile : QDir(":/fonts/fonts/").entryInfoList({ "*.ttf" }))
        if (QFontDatabase::addApplicationFont(fontFile.absoluteFilePath()) < 0)
            qWarning() << "Font loading failed:" << fontFile.fileName();
        else
            qDebug() << fontFile.baseName();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    loadFlonts();

    MainWindow w;
    w.resize(1368, 758);
    w.show();

#ifdef UTJ_DEV_b281444a_cddc_4339_addb_af4c85a53ab5
    const QString root =
#ifdef Q_OS_WIN
            "D:";
#else
            "/home/denis";
#endif // Q_OS_WIN
    const QString &cssStorage = QString("%1/develop/me/utj/resources/css/").arg(root);
#else
    const QString cssStorage(":/css/css/");
#endif // UTJ_DEV_b281444a_cddc_4339_addb_af4c85a53ab5

    CssLoader::attach(&a, cssStorage, QKeySequence("F5"));

    return a.exec();
}
