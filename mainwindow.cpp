#include "mainwindow.h"

#include "centralwidget.h"
#include "titlebar.h"

#include <QPainter>
#include <QStyleOption>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent, Qt::FramelessWindowHint)
    , m_titleBar(new TitleBar(this))
    , m_centralWidget(new CentralWidget(this))
{
    m_titleBar->setObjectName("titleBar");
    QVBoxLayout *root = new QVBoxLayout(this);
    root->setContentsMargins(0, 0, 0, 0);
    root->setSpacing(0);
    root->addWidget(m_titleBar);
    root->addWidget(m_centralWidget);
}
