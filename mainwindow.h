#pragma once

#include <QWidget>

class TitleBar;
class CentralWidget;

class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    TitleBar *m_titleBar;
    CentralWidget *m_centralWidget;
};
