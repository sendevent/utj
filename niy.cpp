#include "niy.h"

#include <QApplication>
#include <QMessageBox>

namespace utj {

void showNIY(const QString &title, QWidget *parent)
{
    QMessageBox::information(parent ? parent : qApp->topLevelWidgets().first(), "NIY",
                             QString("%1: not implemented yet").arg(title));
}

} // ns utj
