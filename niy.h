#pragma once

#include <QApplication>
#include <QFont>
#include <QMessageBox>

namespace utj {

void showNIY(const QString &title, QWidget *parent = nullptr);

} // ns utj
