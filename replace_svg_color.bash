suffix=".svg"
target_dir="$1"
name_mark="$2"

for entry in "$target_dir"/*$suffixs
do
    name_src=`basename "$entry" .svg`
    name_dst="$target_dir/$name_src"_"$name_mark$suffix"
    echo $name_dst
    cp "$entry" "$name_dst"
    sed -i "s/stroke=\"black\"/stroke=\"$3\"/" "$name_dst"
done
