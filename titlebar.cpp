/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "titlebar.h"

#include "niy.h"

#include <QBoxLayout>
#include <QDebug>
#include <QLabel>
#include <QMouseEvent>
#include <QToolButton>

TitleBar::TitleBar(QWidget *parent)
    : QWidget(parent)
    , m_lblIcon(new QLabel(this))
    , m_lblTitle(new QLabel(this))
    , m_btnSettings(new QToolButton(this))
    , m_btnHelp(new QToolButton(this))
    , m_btnMin(new QToolButton(this))
    , m_btnMinMax(new QToolButton(this))
    , m_btnClose(new QToolButton(this))
{
    m_lblTitle->setText(tr("Photo Editor 1.0"));
    m_lblTitle->setContentsMargins(0, 0, 0, 0);

    setFixedHeight(40);
    m_lblIcon->setFixedSize(20, 20);
    m_btnSettings->setFixedSize(22, 22);
    m_btnHelp->setFixedSize(20, 20);
    m_btnMin->setFixedSize(16, 16);
    m_btnMinMax->setFixedSize(12, 12);
    m_btnClose->setFixedSize(12, 12);

    m_lblIcon->setObjectName("lblIcon");
    m_lblTitle->setObjectName("lblTitle");
    m_btnSettings->setObjectName("btnSettings");
    m_btnHelp->setObjectName("btnHelp");
    m_btnMin->setObjectName("btnMin");
    m_btnMinMax->setObjectName("btnMinMax");
    m_btnClose->setObjectName("btnClose");

    QHBoxLayout *horizontalLayout = new QHBoxLayout(this);
    horizontalLayout->setSpacing(0);
    horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
    horizontalLayout->setContentsMargins(11, 10, 15, 9);
    horizontalLayout->setAlignment(Qt::AlignVCenter);

    horizontalLayout->addWidget(m_lblIcon);
    horizontalLayout->addItem(new QSpacerItem(6, 20, QSizePolicy::Fixed, QSizePolicy::Minimum));

    horizontalLayout->addWidget(m_lblTitle);

    horizontalLayout->addWidget(m_btnSettings);
    horizontalLayout->addItem(new QSpacerItem(19, 20, QSizePolicy::Fixed, QSizePolicy::Minimum));

    horizontalLayout->addWidget(m_btnHelp);
    horizontalLayout->addItem(new QSpacerItem(38, 20, QSizePolicy::Fixed, QSizePolicy::Minimum));

    horizontalLayout->addWidget(m_btnMin);
    horizontalLayout->addItem(new QSpacerItem(26, 20, QSizePolicy::Fixed, QSizePolicy::Minimum));

    horizontalLayout->addWidget(m_btnMinMax);
    horizontalLayout->addItem(new QSpacerItem(28, 20, QSizePolicy::Fixed, QSizePolicy::Minimum));

    horizontalLayout->addWidget(m_btnClose);

    connect(m_btnSettings, &QToolButton::clicked, this, &TitleBar::onSettingsClicked);
    connect(m_btnHelp, &QToolButton::clicked, this, &TitleBar::onHelpClicked);
    connect(m_btnMin, &QToolButton::clicked, this, &TitleBar::onMinClicked);
    connect(m_btnMinMax, &QToolButton::clicked, this, &TitleBar::onMinMaxClicked);
    connect(m_btnClose, &QToolButton::clicked, this, &TitleBar::onCloseClicked);
}

void TitleBar::onSettingsClicked()
{
    utj::showNIY("onSettingsClicked", this);
}

void TitleBar::onHelpClicked()
{
    utj::showNIY("onHelpClicked", this);
}

void TitleBar::onMinClicked()
{
    if (auto w = target())
        w->showMinimized();
}

void TitleBar::onMinMaxClicked()
{
    if (auto w = target()) {
        const bool isMax = w->isMaximized();
        isMax ? w->showNormal() : w->showMaximized();
        // TODO: upd btn's icon
    }
}

void TitleBar::onCloseClicked()
{
    if (auto w = target())
        w->close();
}

QWidget *TitleBar::target() const
{
    return parentWidget();
}

void TitleBar::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && geometry().contains(event->pos())) {
        m_click = event->screenPos();
    }

    QWidget::mousePressEvent(event);
}

void TitleBar::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() == Qt::LeftButton && !m_click.isNull()) {
        if (QWidget *w = parentWidget()) {
            const QPointF &delta = event->screenPos() - m_click;
            const QPoint dst(w->pos() + delta.toPoint());
            w->move(dst);
            m_click = event->screenPos();
        }
    }

    QWidget::mouseMoveEvent(event);
}

void TitleBar::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        m_click = QPoint();

    QWidget::mouseReleaseEvent(event);
}

void TitleBar::mouseDoubleClickEvent(QMouseEvent *event)
{
    onMinMaxClicked();
    QWidget::mouseDoubleClickEvent(event);
}

void TitleBar::wheelEvent(QWheelEvent *e)
{
    if (auto w = parentWidget())
        if (e->modifiers() == Qt::AltModifier) {
            const qreal delta = 0.1 * (e->angleDelta().x() > 0 ? -1 : 1);
            w->setWindowOpacity(w->windowOpacity() + delta);
            return;
        }

    QWidget::wheelEvent(e);
}
