/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include <QWidget>

class QLabel;
class QToolButton;

class TitleBar : public QWidget
{
public:
    explicit TitleBar(QWidget *parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent *e) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    void mouseReleaseEvent(QMouseEvent *e) override;
    void mouseDoubleClickEvent(QMouseEvent *e) override;
    void wheelEvent(QWheelEvent *e) override;

protected slots:
    void onSettingsClicked();
    void onHelpClicked();
    void onMinClicked();
    void onMinMaxClicked();
    void onCloseClicked();

private:
    QLabel *m_lblIcon;
    QLabel *m_lblTitle;

    QToolButton *m_btnSettings;
    QToolButton *m_btnHelp;
    QToolButton *m_btnMin;
    QToolButton *m_btnMinMax;
    QToolButton *m_btnClose;

    QPointF m_click;

    QWidget *target() const;
};
