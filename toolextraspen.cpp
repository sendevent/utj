/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "toolextraspen.h"

#include "cmd/cmdprocessor.h"
#include "document/document.h"
#include "document/documentview.h"
#include "document/project.h"
#include "tools/colorpicker.h"
#include "tools/pentool.h"

#include <QBoxLayout>
#include <QColorDialog>
#include <QDebug>
#include <QLabel>
#include <QMenu>
#include <QPainterPath>
#include <QPushButton>
#include <QSlider>
#include <QToolButton>

ToolExtrasPen::ToolExtrasPen(PenTool *penTool, QWidget *parent)
    : QFrame(parent)
    , m_penTool(penTool)
    , m_labelOpacityTitle(new QLabel(tr("Opacity image"), this))
    , m_labelOpacityDisplay(new QLabel(tr("100%"), this))
    , m_labelOutlineColor(new QLabel(tr("Outline color"), this))
    , m_opacitySlider(new QSlider(Qt::Horizontal, this))
    , m_btnColorPicker(new QPushButton(this))
    , m_btnColorDialog(new QToolButton(this))
    , m_clrPicker(new ColorPicker(this))
{
    setObjectName("penToolExtras");
    m_btnColorPicker->setObjectName("btnColorpicker");
    m_btnColorPicker->setCheckable(true);
    connect(m_btnColorPicker, &QToolButton::clicked, this, &ToolExtrasPen::onColorPickerRequested);

    //    QMenu *colorsMenu = new QMenu(this);
    //    colorsMenu->setObjectName("colorDialogBtnMenu");
    //    colorsMenu->setEnabled(false);
    //    for (int i = 0; i < 7; ++i)
    //        colorsMenu->addAction(QString("fake clr#%1").arg(QString::number(i + 1)));
    //    m_btnColorDialog->setMenu(colorsMenu);
    m_btnColorDialog->setPopupMode(QToolButton::MenuButtonPopup);

    m_btnColorDialog->setObjectName("btnColorDialog");
    connect(m_btnColorDialog, &QAbstractButton::clicked, this, &ToolExtrasPen::showColorDialog);

    m_labelOpacityDisplay->setObjectName("opacityDisplay");
    m_labelOpacityDisplay->setFixedSize(52, 26);
    m_labelOpacityDisplay->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

    m_opacitySlider->setFixedSize(194, 26);

    QHBoxLayout *row1 = new QHBoxLayout();
    row1->addWidget(m_opacitySlider);
    row1->addWidget(m_labelOpacityDisplay);
    row1->setSpacing(14);
    row1->setContentsMargins(0, 0, 24, 0);
    row1->setAlignment(Qt::AlignLeft);

    QHBoxLayout *row2 = new QHBoxLayout();
    row2->setSpacing(4);
    row2->setContentsMargins(0, 0, 24, 0);
    row2->addWidget(m_labelOutlineColor);
    row2->addItem(new QSpacerItem(65, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
    row2->addWidget(m_btnColorPicker);
    row2->addWidget(m_btnColorDialog);

    QVBoxLayout *root = new QVBoxLayout(this);
    root->setAlignment(Qt::AlignTop);
    root->setContentsMargins(23, 18, 0, 0);
    root->setSpacing(1);
    root->addWidget(m_labelOpacityTitle);

    root->addLayout(row1);
    root->addItem(new QSpacerItem(2, 15, QSizePolicy::Fixed, QSizePolicy::Minimum));
    root->addLayout(row2);

    setFixedHeight(136);

    root->setSizeConstraint(QLayout::SetMinimumSize);

    m_opacitySlider->setMaximum(100);
    m_opacitySlider->setValue(100);
    connect(m_opacitySlider, &QSlider::valueChanged, this, &ToolExtrasPen::onOpacitySliderMoved);

    connect(m_clrPicker, &ColorPicker::colorPicked, this, &ToolExtrasPen::onColorPicked);
    connect(m_penTool, &PenTool::colorChanged, this, &ToolExtrasPen::updateColorUI);
    updateColorUI(m_penTool->color());
}

ToolExtrasPen::~ToolExtrasPen() {}

void ToolExtrasPen::onOpacitySliderMoved(int opacity)
{
    qDebug() << "onOpacitySliderMoved" << opacity;

    if (const DocumentPtr &doc = Project::currentDoc()) {
        if (doc->opacity() != opacity) {
            connect(doc.data(), &Document::opacityChanged, this, &ToolExtrasPen::onDocumentOpacityChanged,
                    Qt::UniqueConnection);
            CmdProcessor::runCmd(utj::Tool::OpacityChange, { opacity });
        }
    }
}

void ToolExtrasPen::onDocumentOpacityChanged(int opacity)
{
    qDebug() << "onDocumentOpacityChanged" << opacity;
    m_labelOpacityDisplay->setText(tr("%1%").arg(QString::number(opacity)));
    m_opacitySlider->setValue(opacity);
}

void ToolExtrasPen::onColorPickerRequested()
{
    m_clrPicker->togglePicking(Project::mainView());
}

QIcon generate(const QColor &clr)
{
    // TODO: create a prop/setter to be managed via CSS:
    static const QIcon defaultIcon(":/icn/no_color.svg");

    if (!clr.isValid())
        return defaultIcon;

    static const int side = 512;
    QPixmap pm(side, side);
    pm.fill(Qt::transparent);

    const QRect &pm_rect = pm.rect();

    QPainter p(&pm);
    p.setRenderHint(QPainter::Antialiasing);

    QPainterPath path;
    path.addEllipse(pm_rect);
    p.setClipPath(path);

    auto stroke = [&p, &pm_rect, path](qreal w, const QColor &c) {
        p.save();
        const qreal half_w = w / 2.;
        const QRect stroked_r = pm_rect.adjusted(half_w, half_w, -half_w, -half_w);
        QPen pen(c);
        pen.setWidthF(w);
        p.setPen(pen);
        p.drawEllipse(stroked_r);

        p.restore();
    };

    p.fillRect(pm_rect, clr);
    stroke(4., Qt::lightGray);
    stroke(2., Qt::darkGray);

    p.end();

    return QIcon(pm);
}

void ToolExtrasPen::onColorPicked(const QColor &c)
{
    if (c.isValid() && m_penTool->color() != c)
        if (const DocumentPtr doc =
                    CmdProcessor::runCmd(utj::Tool::ChangePenColor, { c, QVariant::fromValue(m_penTool) })) {
        }
}

void ToolExtrasPen::updateColorUI(const QColor &c)
{
    qDebug() << "updateColorUI:" << c.name();
    m_btnColorDialog->setIcon(generate(c));
}

void ToolExtrasPen::showColorDialog()
{
    const QColor &clr = QColorDialog::getColor(m_penTool->color(), this, tr("Chose pen color"));
    onColorPicked(clr);
}
