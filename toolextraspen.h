/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include <QFrame>

class QLabel;
class QSlider;
class QPushButton;
class QToolButton;
class ColorPicker;
class PenTool;

class ToolExtrasPen : public QFrame
{
    Q_OBJECT

public:
    explicit ToolExtrasPen(PenTool *penTool, QWidget *parent = nullptr);
    ~ToolExtrasPen();

private:
    PenTool *m_penTool;
    QLabel *m_labelOpacityTitle { nullptr };
    QLabel *m_labelOpacityDisplay { nullptr };
    QLabel *m_labelOutlineColor { nullptr };
    QSlider *m_opacitySlider { nullptr };

    QPushButton *m_btnColorPicker { nullptr };
    QToolButton *m_btnColorDialog { nullptr };

    ColorPicker *m_clrPicker;

    QColor m_color { Qt::black };

private slots:
    void onOpacitySliderMoved(int);
    void onDocumentOpacityChanged(int);

    void onColorPickerRequested();

    void onColorPicked(const QColor &c);
    void updateColorUI(const QColor &c);

    void showColorDialog();
};
