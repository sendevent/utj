/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "basictool.h"

#include <QAction>

BasicTool::BasicTool(utj::Tool toolType, const QString &title, QObject *parent)
    : QObject(parent)
    , m_tt(toolType)
    , m_title(title)
    , m_active(false)
{
}

BasicTool::~BasicTool() {}

QString BasicTool::title() const
{
    return m_title;
}

bool BasicTool::isActive() const
{
    return m_active;
}

utj::Tool BasicTool::toolType() const
{
    return m_tt;
}

void BasicTool::setActive(bool active)
{
    if (active != m_active) {
        m_active = active;
        emit activationChanged(m_active);
    }
}
