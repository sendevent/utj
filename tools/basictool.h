/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include "tools/tooltype.h"

#include <QObject>

class BasicTool : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool active READ isActive WRITE setActive NOTIFY activationChanged)

public:
    explicit BasicTool(utj::Tool toolType, const QString &title, QObject *parent = nullptr);
    ~BasicTool();

    virtual void initGUI() = 0;

    QString title() const;
    bool isActive() const;
    utj::Tool toolType() const;

public slots:
    void setActive(bool active);

signals:
    void activationChanged(bool);

protected:
    utj::Tool m_tt { utj::Tool::Unknown }; // as for tool type
    QString m_title;
    bool m_active { false };
    QVariantList m_props;
};
