/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "colorpicker.h"

#include "canvasitem.h"

#include <QApplication>
#include <QDebug>
#include <QEvent>
#include <QKeyEvent>
#include <QMouseEvent>

ColorPicker::ColorPicker(QObject *parent)
    : QObject(parent)
{
}

void ColorPicker::togglePicking(DocumentView *onView)
{
    if (m_docView)
        stopPicking();
    else
        startPicking(onView);
}

void ColorPicker::startPicking(DocumentView *onView)
{
    m_docView = onView;
    if (m_docView) {
        m_docView->installEventFilter(this);
        m_docView->item()->setCursor(Qt::CrossCursor);
        qApp->installEventFilter(this);
    }
}

void ColorPicker::stopPicking()
{
    if (m_docView) {
        m_docView->removeEventFilter(this);
        m_docView->item()->unsetCursor();
        m_docView = nullptr;
        qApp->removeEventFilter(this);
    }
}

bool ColorPicker::eventFilter(QObject *obj, QEvent *e)
{
    bool finish = false;
    {
        switch (e->type()) {
        case QEvent::KeyPress: {
            if (auto keyEvent = static_cast<QKeyEvent *>(e)) {
                finish = keyEvent->key() == Qt::Key_Escape;
            }
            break;
        }
        case QEvent::MouseButtonPress:
            if (obj == m_docView) {
                finish = true;
                if (auto mouseEvent = static_cast<QMouseEvent *>(e)) {
                    const QColor &c = m_docView->colorAt(mouseEvent->globalPos());
                    emit colorPicked(c);
                }
            }
            break;
        default:
            break;
        }
    }

    if (finish)
        stopPicking();

    return false;
}
