/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "dynamicinputtool.h"

#include <QAction>
#include <QFrame>
#include <QIcon>
#include <QLabel>
#include <QVBoxLayout>

DynamicInputTool::DynamicInputTool(utj::Tool tt, const QString &title, const QString &icon, QObject *parent)
    : BasicTool(tt, title, parent)
{
    m_action = new QAction(/*QIcon(icon),*/ m_title, this);
    m_action->setCheckable(true);

    connect(m_action, &QAction::toggled, this, &BasicTool::setActive);
    connect(this, &BasicTool::activationChanged, m_action, &QAction::setChecked);
}

/*virtual*/ void DynamicInputTool::initGUI()
{
    QFrame *view = new QFrame();

    view->setMinimumSize(200, 100);
    QVBoxLayout *vBox = new QVBoxLayout(view);
    QLabel *label = new QLabel(QString("<b>%1</b>").arg(title()), view);
    vBox->addWidget(label);
    view->setLayout(vBox);

    m_propsView = view;
}
