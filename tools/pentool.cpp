/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "pentool.h"

#include "toolextraspen.h"

PenTool::PenTool(QObject *parent)
    : DynamicInputTool(utj::Tool::Pen, tr("Pen"), QString(), parent)
    , m_color(QColor::Invalid)
{
}

/*virtual*/ void PenTool::initGUI()
{
    m_propsView = new ToolExtrasPen(this);
}

QColor PenTool::color() const
{
    return m_color;
}

void PenTool::setColor(const QColor &clr)
{
    if (clr != m_color) {
        m_color = clr;
        emit colorChanged(m_color);
    }
}
