/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "toolsbox.h"

#include "tools/dynamicinputtool.h"
#include "tools/toolsfactory.h"

#include <QActionGroup>
#include <QMetaEnum>

ToolsBox::ToolsBox(QObject *parent)
    : QObject(parent)
    , m_actGroup(new QActionGroup(this))
{
}

void ToolsBox::initTools(DocumentView * /*docView*/)
{
    BasicTool *firstTool = nullptr;

    const QMetaEnum &e = QMetaEnum::fromType<utj::Tool>();
    for (int i = 0; i < e.keyCount(); ++i) {
        const utj::Tool tt = static_cast<utj::Tool>(e.value(i));
        if (utj::Tool::Unknown != tt) {
            if (BasicTool *tool = utj::createTool(tt)) {
                if (auto dynamicInput = qobject_cast<DynamicInputTool *>(tool))
                    m_actGroup->addAction(dynamicInput->action());

                m_tools.insert(tt, tool);
                connect(tool, &BasicTool::activationChanged, this, &ToolsBox::onToolToggled);

                if (!firstTool)
                    firstTool = tool;
            }
        }
    }

    firstTool->setActive(true); // TODO: read from settings
}

void ToolsBox::onToolToggled(bool active)
{
    if (active)
        if (const BasicTool *tool = qobject_cast<const BasicTool *>(sender()))
            emit toolActivated(tool->toolType());
}
