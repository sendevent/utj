/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include "tools/tooltype.h"

#include <QMap>
#include <QObject>

class BasicTool;
class QActionGroup;
class DocumentView;

class ToolsBox : public QObject
{
    Q_OBJECT
public:
    explicit ToolsBox(QObject *parent = nullptr);

    const QMap<utj::Tool, BasicTool *> &tools() const { return m_tools; }

    void initTools(DocumentView *docView);

signals:
    void toolActivated(utj::Tool tool);

private:
    QMap<utj::Tool, BasicTool *> m_tools;
    utj::Tool m_currTool { utj::Tool::Unknown };
    QActionGroup *m_actGroup { nullptr };

private slots:
    void onToolToggled(bool active);
};
