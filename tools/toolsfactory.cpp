/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "toolsfactory.h"

#include "tools/dynamicinputtool.h"
#include "tools/pentool.h"

#include <QDebug>
#include <QMap>
#include <QObject>
#define WRN qWarning()

namespace utj {

BasicTool *createTool(utj::Tool tt)
{
    using namespace utj;

    // TODO: move each in a separate class
    static const QMap<Tool, QPair<QString, QString>> decorMap = {
        { Tool::Pen, { QObject::tr("Pen"), "" } },
        { Tool::Arrow, { QObject::tr("Arrow"), ":/icn/icon_Hand_rest.svg" } },
        { Tool::Rect, { QObject::tr("Rect"), ":/icn/Rectangle_rest.svg" } },
        { Tool::Ellipse, { QObject::tr("Ellipse"), ":/icn/Ellipse_rest.svg" } },
        { Tool::Triangle, { QObject::tr("Triangle"), ":/icn/Polygon_rest.svg" } },
        { Tool::Star, { QObject::tr("Star"), ":/icn/favorite_rest.svg" } },
    };

    const auto &paths = decorMap.value(tt, { "", "" });

    BasicTool *tool = nullptr;
    switch (tt) {
    case Tool::Pen: {
        tool = new PenTool();
        tool->initGUI();
        break;
    }
    case Tool::Arrow:
    case Tool::Rect:
    case Tool::Ellipse:
    case Tool::Triangle:
    case Tool::Star: {
        tool = new DynamicInputTool(tt, paths.first, paths.second);
        tool->initGUI();
        break;
    }
    default:
        WRN << "ToolsFactory::createTool: unsupported tool type: " << tt;
        break;
    }

    return tool;
}

} // namespace utj
