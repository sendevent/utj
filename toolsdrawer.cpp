/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#include "toolsdrawer.h"

#include "tools/dynamicinputtool.h"

#include <QBoxLayout>
#include <QDebug>
#include <QLabel>
#include <QPainter>
#include <QStackedWidget>
#include <QStyleOption>
#include <QToolBar>
#include <QToolButton>

ToolsDrawer::ToolsDrawer(QWidget *parent)
    : QFrame(parent)
    , m_toolBar(new QToolBar(this))
    , m_stackWidget(new QStackedWidget(this))
{
    setFixedWidth(309);
    setObjectName("toolsDrawer");
    m_toolBar->setObjectName("drawToolBar");
    m_toolBar->setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonIconOnly);

    QVBoxLayout *rootLayout = new QVBoxLayout(this);
    rootLayout->setContentsMargins(0, 0, 0, 0);
    rootLayout->setSpacing(0);

    QVBoxLayout *toolsLayout = new QVBoxLayout();
    toolsLayout->setContentsMargins(24, 18, 24, 16);
    toolsLayout->setSpacing(19);

    QLabel *drawerTitle = new QLabel(this);
    drawerTitle->setObjectName("drawerTitle");
    drawerTitle->setText(tr("Draw Tools"));

    toolsLayout->addWidget(drawerTitle);
    toolsLayout->addWidget(m_toolBar);

    rootLayout->addLayout(toolsLayout);
    rootLayout->addWidget(m_stackWidget);
    rootLayout->addSpacerItem(new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Expanding));
    rootLayout->setSizeConstraint(QLayout::SetMinimumSize);
}

void ToolsDrawer::addTool(const BasicTool *basicTool)
{
    if (auto tool = qobject_cast<const DynamicInputTool *>(basicTool)) {
        QToolButton *btn = new QToolButton(m_toolBar);
        btn->setDefaultAction(tool->action());
        btn->setObjectName(QString("drawToolBtn%1").arg(tool->title()));
        btn->setToolButtonStyle(Qt::ToolButtonIconOnly);

        m_toolBar->addWidget(btn);

        m_toolsMap.insert(tool->toolType(), m_stackWidget->count());
        m_stackWidget->addWidget(tool->getPropsView());

        connect(tool, &BasicTool::activationChanged, this, &ToolsDrawer::onToolActivated);
    }
}

void ToolsDrawer::onToolActivated()
{
    if (auto tool = qobject_cast<const BasicTool *>(sender())) {
        if (tool->isActive())
            m_stackWidget->setCurrentIndex(m_toolsMap.value(tool->toolType()));
    }
}
