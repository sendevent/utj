/*
   Copyright (C) 2021 Denis Gofman - <sendevent@gmail.com>

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.html>.
*/

#pragma once

#include "tools/basictool.h"

#include <QFrame>
#include <QMap>

class QToolBar;
class QStackedWidget;
class BasicTool;

class ToolsDrawer : public QFrame
{
    Q_OBJECT
public:
    explicit ToolsDrawer(QWidget *parent = nullptr);
    void addTool(const BasicTool *tool);

private:
    QToolBar *m_toolBar { nullptr };
    QStackedWidget *m_stackWidget { nullptr };
    QMap<utj::Tool, int> m_toolsMap {};

private slots:
    void onToolActivated();
};
