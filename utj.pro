QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    canvasitem.cpp \
    centralwidget.cpp \
    cmd/cmdfactory.cpp \
    cmd/cmdopacitychange.cpp \
    cmd/cmdpencolorchange.cpp \
    cmd/cmdprocessor.cpp \
    cssloader.cpp \
    document/document.cpp \
    document/documentview.cpp \
    document/project.cpp \
    main.cpp \
    mainwindow.cpp \
    niy.cpp \
    titlebar.cpp \
    toolextraspen.cpp \
    tools/basictool.cpp \
    tools/colorpicker.cpp \
    tools/dynamicinputtool.cpp \
    tools/pentool.cpp \
    tools/toolsbox.cpp \
    tools/toolsfactory.cpp \
    toolsdrawer.cpp

HEADERS += \
    canvasitem.h \
    centralwidget.h \
    cmd/cmdfactory.h \
    cmd/cmdopacitychange.h \
    cmd/cmdpencolorchange.h \
    cmd/cmdprocessor.h \
    cssloader.h \
    document/document.h \
    document/documentview.h \
    document/project.h \
    mainwindow.h \
    niy.h \
    titlebar.h \
    toolextraspen.h \
    tools/basictool.h \
    tools/colorpicker.h \
    tools/dynamicinputtool.h \
    tools/pentool.h \
    tools/toolsbox.h \
    tools/toolsfactory.h \
    tools/tooltype.h \
    toolsdrawer.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources/rsc.qrc

DISTFILES += \
    resources/css/app.css \
    resources/css/colordialogbutton.css \
    resources/css/colorpickerbutton.css \
    resources/css/copybutton.css \
    resources/css/drawtoolbuttons.css \
    resources/css/filemenu.css \
    resources/css/opacityslider.css \
    resources/css/ribbon.css \
    resources/css/titlebar.css \
    resources/css/toolsdrawer.css
